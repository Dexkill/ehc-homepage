var currentTag = "home";

function setPage(tag){
    
    currentTag = tag;
    
    if(tag.indexOf("showreport") > -1){
        var repId = tag.split("|")[1];
        
        if(repId != ""){
            showReport(repId);
            return;
        }
    }
    
    if(tag.indexOf("report") > -1){
        var pref = tag.split("|")[1];
        
        if(pref != ""){
            document.getElementById("header-text").innerHTML = " Spielberichte";

            $("#content").fadeOut(100, function(){
                $("#content").load( "sites/view_reports.php?team="+pref, function(){
                    $("#content").fadeIn(100);
                    updateHash(tag);
                });
            });
            return;
        }
    }
    
    var pages = {
                        home : ["Home", "home.php"],
                        reports : ["Spielberichte", "view_reports.php"],
                        aktuell_ls : ["Aktuell: Letzte 30 Spiele", "aktuell_ls.php"],
                        aktuell_ns : ["Aktuell: Nächste 30 Spiele", "aktuell_ns.php"],
                        aktuell_all : ["Aktuell: Alle Spiele", "aktuell_all.php"],
                        aktuell_heim : ["Aktuell: Alle Heimspiele", "aktuell_heim.php"],
                        teams_c_jugend : ["Teams: Männliche C-Jugend", "teams_c_jugend.php"],
                        teams_damen : ["Teams: Damenmannschaft", "teams_damen.php"],
                        teams_herren : ["Teams: Herrenmannschaft", "teams_herren.php"],
                        trainingszeiten : ["Verein: Trainingszeiten", "trainingszeiten.php"],
                        anfahrt : ["Anfahrt", "anfahrt.php"],
                        haftungsausschluss : ["Haftungsausschluss", "haftungsausschluss.php"],
                        impressum : ["Impressum", "impressum.php"]
                };
    
    if(tag != undefined && tag != "" && pages.hasOwnProperty(tag)){
        document.getElementById("header-text").innerHTML = "" + read_prop(pages,tag)[0];
    
        $("#content").fadeOut(200, function(){
            $("#content").load( "sites/" + read_prop(pages,tag)[1], function(){
                $("#content").fadeIn(500);
               updateHash(tag);
            });
        });
    }else{
        setPage("home");
    }
 
}
window.onload = function(){
    $('[data-toggle="tooltip"]').tooltip();
    forceReload();
    $("#cont").load("main_content.php");
};

function updateHash(value){
    document.location.hash = value;
}

function goBack() {
    if(history.length > 0){
        pageHistory.pop();
        setPage(pageHistory[pageHistory.length - 1]);
    }else{
        setPage("home");
    }
}

function getInnerPage(){
    var what_to_do = document.location.hash;  
    
    if(what_to_do != null && what_to_do != ""){
        setPage(what_to_do.split("#")[1]);
    }else{
        setPage("home");
    }
}

function showReport(id){
    document.getElementById("header-text").innerHTML = "Spielbericht";
    
    $("#content").fadeOut(200, function(){
        $("#content").load( "sites/report.php?reportId=" + id, function(){
            $("#content").fadeIn(500);
            updateHash("showreport|" + id);
        });
    });
}

function forceReload(){
    $("#cont").load("main_content.php");
}

function reloadPage(){
    setPage(currentTag);
}

function read_prop(obj, prop) {
    return obj[prop];
}

function action_userLogin(){
    
    var email    = $("#login_email").val();
    var password = $("#login_pwd").val();
    
    $.post( "background.php", {login: true, email: email, password: password}, function(data) {
        
        
        data = JSON.parse(data);
        
        if(data["type"] == 0){
            $('#loginModal').modal('hide');
            forceReload();
        }
        $("#ratingTitle").html(data["title"]);
        $("#ratingBody").html(data["msg"]);
        
        $('#ratingModal').modal('show');
    });
    
}
function action_logout(){
    
    $.post( "background.php", {logout: true}, function(data) {
        
        data = JSON.parse(data);
        
        if(data["type"] == 0){
            $('#logoutModal').modal('hide');
            forceReload();
        }
        
        $("#ratingTitle").html(data["title"]);
        $("#ratingBody").html(data["msg"]);
        
        $('#ratingModal').modal('show');
    });
    
}

function action_createReport(){
    
    var teamID  = document.getElementById("createReport_team").options[document.getElementById("createReport_team").selectedIndex].value;
    var title   = document.getElementById("createReport_titleInput").value;
    var date    = document.getElementById("createReport_dateInput").value;
    var content = document.getElementById("createReport_contentInput").value;
    
    
    
    $.post( "background.php", {createReport: true, teamID: teamID, title: title, date: date, content: content}, function(data) {
        
        data = JSON.parse(data);
        
        if(data["type"] == 0){
            $('#createReportModal').modal('hide');
            forceReload();
        }
        
        $("#ratingTitle").html(data["title"]);
        $("#ratingBody").html(data["msg"]);
        
        $('#ratingModal').modal('show');
    });
    
}

function action_deleteReport(){
    
    var id = document.getElementById("deleteReport_id").value;
    
    $.post( "background.php", {deleteReport: true, reportID: id}, function(data) {
        
        data = JSON.parse(data);
        
        if(data["type"] == 0){
            $('#deleteReportModal').modal('hide');
            forceReload();
        }
        
        $("#ratingTitle").html(data["title"]);
        $("#ratingBody").html(data["msg"]);
        
        $('#ratingModal').modal('show');
    });   
}

function action_updateReport(){
    
    var id = document.getElementById("updateReport_id").value;
    var teamID = document.getElementById("updateReport_team").options[document.getElementById("updateReport_team").selectedIndex].value;
    var title = document.getElementById("updateReport_titleInput").value;
    var date = document.getElementById("updateReport_dateInput").value;
    var content = document.getElementById("updateReport_contentInput").value;
    
    $.post( "background.php", {updateReport: true, reportID: id, teamID: teamID, title: title, date: date, content: content}, function(data) {
        
        data = JSON.parse(data);
        
        if(data["type"] == 0){
            $('#updateReportModal').modal('hide');
            forceReload();
        }
        
        $("#ratingTitle").html(data["title"]);
        $("#ratingBody").html(data["msg"]);
        
        $('#ratingModal').modal('show');
    });
    
}
var address = "";
var geocoder;
var map;
var infoWindow;
var hallenName;
var hallenStrasse;
var hallenOrt;
var gespannName;
function showMapsLocation(hn, hs, ho, gn){
    
    hallenName = hn;
    hallenStrasse = hs;
    hallenOrt = ho;
    gespannName = gn;
    
    geocoder = new google.maps.Geocoder();
    
    map = new google.maps.Map(document.getElementById('vdm_map'), {
        center: {lat: -34.397, lng: 150.644},
        zoom: 15
      });
    
    infoWindow = new google.maps.InfoWindow({map: map});
    
    $("#vdm_austragungsort").html(hallenName + ", " + hallenStrasse + ", " + hallenOrt);
    $("#vdm_schiedsrichter").html(gespannName);
    $('#viewDetailsModal').modal('show');
    
    address = hallenStrasse + ", " + hallenOrt;
    
    setTimeout(function() {reloadMap();}, 3000);
}

function reloadMap(){
    geocoder.geocode( { 'address': address}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          map.setCenter(results[0].geometry.location);
          var marker = new google.maps.Marker({
              map: map,
              position: results[0].geometry.location
          });
        } else {
          alert('Geocode was not successful for the following reason: ' + status);
        }
          
          
          infoWindow.setPosition(results[0].geometry.location);
          infoWindow.setContent("<h2><i class='fa fa-compass'></i> Austragungsort</h2> <br> <b>Hallenname:</b><br> <i>" + hallenName + "</i><br><br> <b>Adresse:</b><br>" + hallenStrasse + ", " + hallenOrt);
      });
    
    google.maps.event.trigger(map, 'resize');
        
}

function codeAddress() {
  
}

function populateUpdateMordal(id){
    $.post( "background.php", {getReport: true, reportID: id}, function(data) {
        data = JSON.parse(data);

        $('#updateReport_team').val(data["teamID"]);
        $('#updateReport_titleInput').val(data["headline"]);
        $('#updateReport_dateInput').val(data["date"]);
        $('#updateReport_contentInput').val(data["content"]);
        
    });
}