<!DOCTYPE html>
<html>
    <head>
        <title>EHC</title>
        <meta charset="utf-8"/>
        <meta 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/bootstrap-theme.min.css"/>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
        <link rel="stylesheet" href="css/font-awesome.min.css"/>
        <link rel="stylesheet" href="css/highslide.css"/>
        <link rel="icon" href="images/favicon.ico" type="image/x-icon"> 
        
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAR1do16RuB7h_EuY9Jxm0xHdkf46BdWdg" type="text/javascript"></script>
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/highslide.min.js"></script>
        <script src="js/index.js"></script>
    </head>
    
    <body>
    
        <div id="cont">Please enable JavaScript!</div>
        
    </body>
</html>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Sign in</h4>
      </div>
      <div class="modal-body">

          <div class="form-group">
            <label for="login_email">Email address</label>
            <input name="email" type="email" class="form-control" id="login_email" placeholder="Email">
          </div>
          <div class="form-group">
            <label for="login_pwd">Password</label>
            <input name="password" type="password" class="form-control" id="login_pwd" placeholder="Password">
          </div>
          <div id="RecaptchaField1"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <input name="login" type="submit" class="btn btn-primary" value="Login" onclick="action_userLogin();"/>
      </div>
    </div>
  </div>
</div>

<?php if($user->login){ ?>

<div class="modal fade" id="deleteReportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Spielbericht löschen</h4>
      </div>
      <div class="modal-body">
          Bist du dir sicher das du diesen Beitrag löschen möchtest?
      </div>
      <div class="modal-footer">
        <input type="hidden" id="deleteReport_id"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Nein</button>
        <input name="logout" type="submit" class="btn btn-danger" value="Ja" onclick="action_deleteReport();"/>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Logout</h4>
      </div>
      <div class="modal-body">
          Bist du dir sicher das du dich abmelden willst?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Nein</button>
        <input name="logout" type="submit" class="btn btn-danger" value="Ja" onclick="action_logout();"/>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="createReportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Spielbericht erstellen</h4>
      </div>
      <div class="modal-body">
          <div id="formWrapperItem">
            Team: <select id="createReport_team">
                <?php

                    $query = "SELECT id, name, prefab FROM teams";
                    $result = mysql_query($query);

                    while($row = mysql_fetch_assoc($result)){
                ?>
                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></option>
                <?php
                    }
                ?>  
            </select> 
          </div>
          
          <div id="formWrapperItem">
              <input id="createReport_titleInput" class="titleInput" type="text" placeholder="Titel"/>
          </div>
          
          <div id="formWrapperItem">
              <input id="createReport_dateInput" class="titleInput" type="text" placeholder="JJJJ-MM-DD"/>
          </div>
          
          <div id="formWrapperItem">
                <textarea style="resize: vertical;" id="createReport_contentInput" class="contentInput" placeholder="Inhalt"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Abbr.</button>
        <input name="logout" type="submit" class="btn btn-primary" value="Erstellen" onclick="action_createReport();"/>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="updateReportModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Spielbericht erstellen</h4>
      </div>
      <div class="modal-body">
          <div id="formWrapperItem">
            Team: <select id="updateReport_team">
                <?php

                    $query = "SELECT id, name, prefab FROM teams";
                    $result = mysql_query($query);

                    while($row = mysql_fetch_assoc($result)){
                ?>
                    <option value="<?php echo $row["id"]; ?>"><?php echo $row["name"]; ?></option>
                <?php
                    }
                ?>  
            </select> 
          </div>
          
          <div id="formWrapperItem">
              <input id="updateReport_titleInput" class="titleInput" type="text" placeholder="Titel"/>
          </div>
          
          <div id="formWrapperItem">
              <input id="updateReport_dateInput" class="titleInput" type="text" placeholder="JJJJ-MM-DD"/>
          </div>
          
          <div id="formWrapperItem">
                <textarea style="resize: vertical;" id="updateReport_contentInput" class="contentInput" placeholder="Inhalt"></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" id="updateReport_id"/>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Abbr.</button>
        <input name="logout" type="submit" class="btn btn-warning" value="Updaten" onclick="action_updateReport();"/>
      </div>
    </div>
  </div>
</div>

<?php } ?>

<div id="viewDetailsModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h2>Spieldetails</h2>
        </div>
        <div class="modal-body">
            <div style="width: 100%; height: 480px;"id="vdm_map"></div>
            <p><b>Austragungsort:</b> <span id="vdm_austragungsort"></span></p>
            <p><b>Schiedsrichter:</b> <span id="vdm_schiedsrichter"></span></p>
        </div>
    </div>
  </div>
</div>

<div id="ratingModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" id="ratingTitle"></h4>
        </div>
        <div class="modal-body" id="ratingBody">
        </div>
    </div>
  </div>
</div>