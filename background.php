<?php
require_once('user.php');
require_once('msg.php');
require_once('functions.php');
require_once('connection.php');

session_start();

if(!isset($_SESSION['user'])){
    $_SESSION['user'] = serialize(new User());
}

$user = unserialize($_SESSION['user']);

$msg = new Msg("Error", "No action!", 1);

//Listeners:
if(isset($_POST['login'])){
    if(!$user->login && $_POST['password']){
        $email = $_POST['email'] ? $_POST['email'] : "";
        $password = $_POST['password'];

        $msg = login($user, $email, $password);
    }else{
        $msg = new Msg("Error", "Unallowed action!", 1);
    }
}

if(isset($_POST['logout'])){
    if($user->login){
        $msg = logout();
        $user = new User();
    }else{
        $msg = new Msg("Error", "Unallowed action!", 1);
    }
}

if(isset($_POST['createReport'])){
    if($user->login){
        $teamID = $_POST['teamID'];
        $title = $_POST['title'];
        $date = $_POST['date'];
        $content = $_POST['content'];
        
        $msg = createReport($user, $teamID, $title, $date, $content);
    }else{
        $msg = new Msg("Error", "Unallowed action!", 1);
    }
}

if(isset($_POST['updateReport'])){
    if($user->login){
        $id = $_POST['reportID'];
        $teamID = $_POST['teamID'];
        $title = $_POST['title'];
        $date = $_POST['date'];
        $content = $_POST['content'];
        
        $msg = updateReport($user, $id, $teamID, $title, $date, $content);
    }else{
        $msg = new Msg("Error", "Unallowed action!", 1);
    }
}

if(isset($_POST['getReport'])){
    if($user->login){
        $id = $_POST['reportID'];
        
        echo getReport($user, $id);
        return;
    }else{
        $msg = new Msg("Error", "Unallowed action!", 1);
    }
}


if(isset($_POST['deleteReport'])){
    if($user->login){
        $id = $_POST['reportID'];
        
        $msg = deleteReport($user, $id);
    }else{
        $msg = new Msg("Error", "Unallowed action!", 1);
    }
}

$_SESSION['user'] = serialize($user);
echo $msg->asJson();
return;