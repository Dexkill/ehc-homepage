<?php
const VEREINSNUMMER = "1310212036";


function getAll(){
    chdir(__DIR__);
    
    $wtagekurz = array("So",
                        "Mo",
                        "Die",
                        "Mi",
                        "Do",
                        "Fr",
                        "Sa");
    
    $xml = simplexml_load_string(file_get_contents("xmlData/all.xml"));

    $return = "";

    $return .= "<table class='table table-striped table-bordered'>"
            . "<thead>
                <tr>
                    <th>Datum</th>
                    <th>Heim : Gast</th>
                    <th>Ergeniss</th>
                    <th>Punkte</th>
                </tr>
                </thead>
                <tbody>";

    $index = 0;
    foreach($xml->Spiel as $spiel){
        $index++;

        $date = date('d.m.Y', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        $estDate = date('Y.m.d', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        $time = date('H:i', strtotime (substr(($spiel->SpielVon), 11)));
        
        if (utf8_decode ($spiel->HeimNr) == VEREINSNUMMER) {
            $heim = v_rename($spiel);
        }else{
            $heim = $spiel->Heim;
        }
        
        if (utf8_decode ($spiel->GastNr) == VEREINSNUMMER) {
            $gast = v_rename($spiel);
        }else{ 
            $gast = $spiel->Gast;
        }     
        
        $wochentag = $wtagekurz[date("w", strtotime (substr(($spiel->SpielDatum), 0, 10)))];
        $done = '<i title="Geplant" class="fa fa-square-o pull-right"></i>';
        $now = new DateTime();
        $now = $now->format('Y.m.d');
        if($now > $estDate){
            $done = '<i title="Abgeschlossen" class="fa fa-check-square-o pull-right" desv></i>';
        }
        
        if($now > $estDate){
            if((int)$spiel->Punkte1 > (int)$spiel->Punkte2){
                $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
            }elseif((int)$spiel->Punkte1 < (int)$spiel->Punkte2){
                $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
            }else{
                $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
                $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
            }
        }
        
        $return .= "<tr>";
        $return .= "<td>$wochentag, <b>$date</b> <span class='visible-xs'><br></span>um <b>$time</b> Uhr $done</td>";
        $return .= "<td> $heim <span class='visible-xs'><b><i>gegen</i></b><br></span><span class='hidden-xs'>:</span> $gast </td>";
        $return .= "<td> $spiel->Tore1 : $spiel->Tore2 </td>";
        $return .= "<td> $spiel->Punkte1 : $spiel->Punkte2 </td>";
        $return .= "</tr>";
    }

    $return .= "</tbody></table>";
    
    return $return;
        
}

function getAllHeim(){
    chdir(__DIR__);
    
    $wtagekurz = array("So",
                        "Mo",
                        "Die",
                        "Mi",
                        "Do",
                        "Fr",
                        "Sa");
    
    $xml = simplexml_load_string(file_get_contents("xmlData/all.xml"));

    $return = "";

    $return .= "<table class='table table-striped table-bordered'>"
            . "<thead>
                <tr>
                    <th>Datum</th>
                    <th>Heim : Gast</th>
                    <th>Ergeniss</th>
                    <th>Punkte</th>
                </tr>
                </thead>
                <tbody>";

    $index = 0;
    foreach($xml->Spiel as $spiel){
        $index++;
        
        $date = date('d.m.Y', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        $estDate = date('Y.m.d', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        $time = date('H:i', strtotime (substr(($spiel->SpielVon), 11)));
        
        if (utf8_decode ($spiel->HeimNr) == VEREINSNUMMER) {
            $heim = v_rename($spiel);
        }else{
            continue;
        }
        
        if (utf8_decode ($spiel->GastNr) == VEREINSNUMMER) {
            $gast = v_rename($spiel);
        }else{ 
            $gast = $spiel->Gast;
        } 
        
        $done = '<i title="Geplant" class="fa fa-square-o pull-right"></i>';
        $now = new DateTime();
        $now = $now->format('Y.m.d');
        if($now > $estDate){
            $done = '<i title="Abgeschlossen" class="fa fa-check-square-o pull-right" desv></i>';
        }
        
        if($now > $estDate){
            if((int)$spiel->Punkte1 > (int)$spiel->Punkte2){
                $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
            }elseif((int)$spiel->Punkte1 < (int)$spiel->Punkte2){
                $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
            }else{
                $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
                $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
            }
        }
        
        $wochentag = $wtagekurz[date("w", strtotime (substr(($spiel->SpielDatum), 0, 10)))];
        
        $return .= "<tr>";
        $return .= "<td>$wochentag, <b>$date</b> <span class='visible-xs'><br></span>um <b>$time</b> Uhr $done</td>";
        $return .= "<td> $heim <span class='visible-xs'><b><i>gegen</i></b><br></span><span class='hidden-xs'>:</span> $gast </td>";
        $return .= "<td> $spiel->Tore1 : $spiel->Tore2 </td>";
        $return .= "<td> $spiel->Punkte1 : $spiel->Punkte2 </td>";
        $return .= "</tr>";
    }

    $return .= "</tbody></table>";
    
    return $return;
        
}

function getLetzteSpiele($limit){
    chdir(__DIR__);
    
    $wtagekurz = array("So",
                        "Mo",
                        "Die",
                        "Mi",
                        "Do",
                        "Fr",
                        "Sa");
    
    $xml = simplexml_load_string(file_get_contents("xmlData/letzten30.xml"));

    $return = "";

    $return .= "<table class='table table-striped table-bordered'>"
            . "<thead>
                <tr>
                    <th>Datum</th>
                    <th>Heim : Gast</th>
                    <th>Ergeniss</th>
                    <th>Punkte</th>
                </tr>
                </thead>
                <tbody>";

    $index = 0;
    foreach($xml->Spiel as $spiel){
        $index++;
        if($index >= $limit){
            break;
        }

        $date = date('d.m.Y', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        
        if (utf8_decode ($spiel->HeimNr) == VEREINSNUMMER) {
            $heim = v_rename($spiel);
        }else{
            $heim = $spiel->Heim;
        }
        
        if (utf8_decode ($spiel->GastNr) == VEREINSNUMMER) {
            $gast = v_rename($spiel);
        }else{ 
            $gast = $spiel->Gast;
        }     
        
        if((int)$spiel->Punkte1 > (int)$spiel->Punkte2){
            $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
        }elseif((int)$spiel->Punkte1 < (int)$spiel->Punkte2){
            $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
        }else{
            $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
            $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
        }
        
        $wochentag = $wtagekurz[date("w", strtotime (substr(($spiel->SpielDatum), 0, 10)))];
        
        $return .= "<tr>";
        $return .= "<td> $wochentag, <b>$date</b> </td>";
        $return .= "<td> $heim <span class='visible-xs'><b><i>gegen</i></b><br></span><span class='hidden-xs'>:</span> $gast </td>";
        $return .= "<td> $spiel->Tore1 : $spiel->Tore2 </td>";
        $return .= "<td> $spiel->Punkte1 : $spiel->Punkte2 </td>";
        $return .= "</tr>";
    }

    $return .= "</tbody></table>";
    
    return $return;
        
}

function getNaechstenSpiele($limit){
    chdir(__DIR__);
    
    $wtagekurz = array("So",
                        "Mo",
                        "Die",
                        "Mi",
                        "Do",
                        "Fr",
                        "Sa");
    
    $xml = simplexml_load_string(file_get_contents("xmlData/naechsten30.xml"));

    $return = "";

    $return .= "<table class='table table-striped table-bordered'>"
            . "<thead>
                <tr>
                    <th>Datum</th>
                    <th>Heim : Gast</th>
                    <th>Optionen</th>
                </tr>
                </thead>
                <tbody>";

    $index = 0;
    foreach($xml->Spiel as $spiel){
        $index++;
        if($index >= $limit){
            break;
        }
        
        $date = date('d.m.Y', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        $time = date('H:i', strtotime (substr(($spiel->SpielVon), 11)));
        
        if (utf8_decode ($spiel->HeimNr) == VEREINSNUMMER) {
            $heim = v_rename($spiel);
        }else{
            $heim = $spiel->Heim;
        }
        
        if (utf8_decode ($spiel->GastNr) == VEREINSNUMMER) {
            $gast = v_rename($spiel);
        }else{ 
            $gast = $spiel->Gast;
        }     
        
        $wochentag = $wtagekurz[date("w", strtotime (substr(($spiel->SpielDatum), 0, 10)))];
        
        $inDays = getInDays($date);

        
        $return .= "<tr>";
        if(!$inDays){
            $return .= "<td>$wochentag, <b>$date</b> <span class='visible-xs'><br></span>um <b>$time</b> Uhr </td>";
        }else{
            $return .= "<td>➤ <b>$inDays</b> ($wochentag), <b>$date</b> <span class='visible-xs'><br></span>um <b>$time</b> Uhr </td>";
        }
        $return .= "<td> $heim <span class='visible-xs'><b><i>gegen</i></b><br></span><span class='hidden-xs'>:</span> $gast </td>";
        $return .= "<td><p class='btn btn-info' onclick='showMapsLocation(&#39;$spiel->HallenName&#39;, &#39;$spiel->HallenStrasse&#39;,
                        &#39;$spiel->HallenOrt&#39;, &#39;$spiel->GespannName&#39;);'><i class='fa fa-location-arrow'></i></p></td>";
        $return .= "</tr>";
    }

    $return .= "</tbody></table>";
    
    return $return;
        
}

function getSpiele($mannschaftsName, $datei){
    
    chdir(__DIR__);
    
    $wtagekurz = array("So",
                        "Mo",
                        "Die",
                        "Mi",
                        "Do",
                        "Fr",
                        "Sa");
    
    $xml = simplexml_load_string(file_get_contents("xmlData/$datei"));

    $return = "<small>".$xml->Spielklasse->Name."</small>";

    $return .= "<table class='table table-striped table-bordered'>"
            . "<thead>
                <tr>
                    <th>Datum</th>
                    <th>Heim : Gast</th>
                    <th>Ergeniss</th>
                    <th>Punkte</th>
                    <th>Optionen</th>
                </tr>
                </thead>
                <tbody>";

    $index = 0;
    foreach($xml->Spiel as $spiel){
        if(!preg_match("/Erndtebrücker HC/u", $spiel->Heim) && !preg_match("/Erndtebrücker HC/u",$spiel->Gast)){
            continue;
        }
        
        $index++;

        $heim = $spiel->Heim;
        $gast = $spiel->Gast;
        
        if(preg_match("/Erndtebrücker HC/u", $heim)){
            $heim = "<span id='ehcTag'>EHC <span>$mannschaftsName</span></span>";
        }elseif(preg_match("/Erndtebrücker HC/u", $gast)){
            $gast = "<span id='ehcTag'>EHC <span>$mannschaftsName</span></span>";
        }
        
        if((int)$spiel->Punkte1 > (int)$spiel->Punkte2){
            $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
        }elseif((int)$spiel->Punkte1 < (int)$spiel->Punkte2){
            $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
        }else{
            $heim = "<i class='fa fa-trophy'></i> <u>".$heim."</u>";
            $gast = "<i class='fa fa-trophy'></i> <u>".$gast."</u>";
        }
        
        $date = date('d.m.Y', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        $estDate = date('Y.m.d', strtotime (substr(($spiel->SpielDatum), 0, 10)));
        
        $done = '<i title="Geplant" class="fa fa-square-o pull-right"></i>';
        
        $wochentag = $wtagekurz[date("w", strtotime (substr(($spiel->SpielDatum), 0, 10)))];
        
        
        $now = new DateTime();
        $now = $now->format('Y.m.d');
        if($now > $estDate){
            $done = '<i title="Abgeschlossen" class="fa fa-check-square-o pull-right" desv></i>';
        }
        
        $return .= "<tr>";
            $return .= "<td><span class='hidden-xs'>$wochentag, </span>$date $done</td>";
            $return .= "<td> $heim <span class='visible-xs'><b><i>gegen</i></b><br></span><span class='hidden-xs'>:</span> $gast </td>";
            $return .= "<td> $spiel->Tore1 : $spiel->Tore2 </td>";
            $return .= "<td style='color:gray;'> $spiel->Punkte1 : $spiel->Punkte2 </td>";
            $return .= "<td><p class='btn btn-info' onclick='showMapsLocation(&#39;$spiel->HallenName&#39;, &#39;$spiel->HallenStrasse&#39;,
                        &#39;$spiel->HallenOrt&#39;, &#39;$spiel->GespannName&#39;);'><i class='fa fa-location-arrow'></i></p></td>";
        $return .= "</tr>";
    }

    $return .= "</tbody></table>";
    
    return $return;
        
}

function getTabelle($mannschaftsName, $datei){
    
    chdir(__DIR__);
    
    $xml = simplexml_load_string(file_get_contents("xmlData/$datei"));

    $return = "<small>".$xml->Spielklasse->Name."</small>";

    $return .= "<table class='table table-striped table-bordered'>"
            . "<thead>
                <tr>
                    <th>Platz</th>
                    <th>Verein</th>
                    <th>Begegnungen</th>
                    <th>Tore : Gegentore</th>
                    <th>Differenz</th>
                    <th>Punkte : Gegenpunkte</th>
                </tr>
                </thead>
                <tbody>";

    $index = 0;
    foreach($xml->Platzierung as $platz){
        $index++;

        $ehc = false;
        $name = $platz->Name;
        
        if(preg_match("/Erndtebrücker HC/u", $name)){
            $ehc = true;
            $name = "<span id='ehcTag' style='text-shadow: 0px 0px 5px black;'>EHC <span>$mannschaftsName</span></span>";
        }
        if($ehc){
         $return .= "<tr style='background: #003D99 !important; color: white;'>";  
        }else{
         $return .= "<tr>";
        }
            $return .= "<td> $index</td>";
            $return .= "<td> $name</td>";
            $return .= "<td> $platz->Spiele / $platz->SpieleInsgesamt</td>";
            $return .= "<td> $platz->TorePlus : $platz->ToreMinus </td>";
            $return .= "<td style='color:gray;'> ".($platz->TorePlus - $platz->ToreMinus)." </td>";
            $return .= "<td> $platz->PunktePlus : $platz->PunkteMinus </td>";
        $return .= "</tr>";
    }

    $return .= "</tbody></table>";
    
    return $return;
        
}

function v_rename($spiel){
    // Teams
    $damen = "Damen";
    $herren = "Herren";
    $maj = "M&auml;nnl. A-Jgd.";
    $mbj = "M&auml;nnl. B-Jgd.";
    $mcj = "M&auml;nnl. C-Jgd.";
    $mdj = "M&auml;nnl. D-Jgd.";
    $waj = "Weibl. A-Jgd.";
    $wbj = "Weibl. B-Jgd.";
    $wcj = "Weibl. C-Jgd.";
    $wdj = "Weibl. D-Jgd.";
    $gdj = "Gem. D-Jgd.";
    
    // Verlinkung mit EHC
    $damenlink = "<a onclick='setPage(`teams_damen`);'><b><font color='#fca000'>EHC</font> ".$damen."</b></a>";
    $herrenlink = "<a onclick='setPage(`teams_herren`);'><b><font color='#fca000'>EHC</font> ".$herren."</b></a>";
    $mcjlink = "<a onclick='setPage(`teams_c_jugend`);'><b><font color='#fca000'>EHC</font> ".$mcj."</b></a>";
    $wajlink = "<a><b><font color='#fca000'>EHC</font> ".$waj."</b></a>";
    $wbjlink = "<a onclick='setPage(`teams_wb_jugend`);'><b><font color='#fca000'>EHC</font> ".$wbj."</b></a>";
    $gdjlink = "<a onclick='setPage(`teams_d_jugend`);'><b><font color='#fca000'>EHC</font> ".$gdj."</b></a>";
    
    if (preg_match("/1210/", "$spiel->LigaName")) { return $damenlink; }
    else if (preg_match("/1201/", "$spiel->LigaName")) { return $herrenlink; }

    // Saison 2013/2014
    //else if (preg_match("/1. Runde zu spielen bis zum 31.08.2014/", "$spiel->LigaName")) { echo $herrenlink." <img src='http://erndtebrueckerhc.de/cms/_sis/icons/pokal.png' border='0' title='Pokalspiel'></b>"; }
    //else if (preg_match("/zu spielen bis zum 01.11.2014/", "$spiel->LigaName")) { echo $herrenlink." <img src='http://erndtebrueckerhc.de/cms/_sis/icons/pokal.png' border='0' title='Pokalspiel'></b>"; }
    //else if (preg_match("/2. Runde/", "$spiel->LigaName")) { echo $herrenlink." <img src='http://erndtebrueckerhc.de/cms/_sis/icons/pokal.png' border='0' title='Pokalspiel'></b>"; }

    //else if (preg_match("/zu spielen bis zum 25.01.2014/", "$spiel->LigaName")) { echo $damenlink." <img src='http://erndtebrueckerhc.de/cms/_sis/icons/pokal.png' border='0' title='Pokalspiel'></b>"; }

    //else if (preg_match("/1. Runde 30/", "$spiel->LigaName")) { echo $damenlink." <img src='http://erndtebrueckerhc.de/cms/_sis/icons/pokal.png' border='0' title='Pokalspiel'></b>"; }
    // Verlinkung ohne EHC
    

    else if (preg_match("/1235/", "$spiel->LigaName")) { return $mcjlink; } 
    else if (preg_match("/1262/", "$spiel->LigaName")) { return $wbjlink; } 

    else { return ($spiel->LigaName); }      
}

function getInDays($time)
{
    /*
    $time = strtotime($time) - time(); // to get the time since that moment

    if ($time < 86400){
        return "Heute";
    }else if($time < 86400 * 2){
        return "Morgen";
    }else{
        return false;
    }*/
    
    $date = date('Y-m-d', strtotime($time));
    $today = date('Y-m-d');
    $tomorrow = date('Y-m-d', strtotime('tomorrow')); 
    $day_after_tomorrow = date('Y-m-d', strtotime('tomorrow + 1 day'));

    if ($date == $today) {
      return "Heute";
    } else if ($date == $tomorrow) {
      return "Morgen";
    }
    
    return false;
}
