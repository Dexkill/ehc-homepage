<?php
function login($user, $email, $password){
    if($email == "")
    {
        return new Msg("Error", "Fill all fields!", 1, "$('#loginModal').modal('show');");
    }

    
    $query = sprintf("SELECT id, name, email, role, deleted FROM users WHERE email = '%s' AND password = '%s' AND deleted = 0",      
    mysql_real_escape_string($email),
    mysql_real_escape_string(md5($password."aad-Lkz542-a1x")));
    
    $result = mysql_query($query);
    
    $menge = mysql_num_rows($result);

    if($menge > 0)
    {
        $resultArray = mysql_fetch_array($result);
        
        $user->update($resultArray["id"], $resultArray["name"], $resultArray["email"], $resultArray["role"], true);
        
        
        return new Msg("Sign in", "You are now signed in!", 0);
    }else{
        return new Msg("Error", "This comination seems to be wrong!", 1, "$('#loginModal').modal('show');");
    }
    return new Msg("Error", "An unknown error occured. Please try again later!", 1, "$('#loginModal').modal('show');");
}

function logout(){
    session_destroy();
    return new Msg("Logout", "You are now logged out!", 0);
}

function createReport($user, $teamID, $title, $date, $content){
    if(!$teamID || !$title || !$content || $title == "" || $content == "" || $date == ""){
        return new Msg("Fehler", "Bitte alles ausfüllen!", 1);
    }
    
    $title = htmlentities($title, ENT_QUOTES);
    $content = htmlentities($content, ENT_QUOTES);
    $date = htmlentities($content, ENT_QUOTES);
    
    $query = "INSERT INTO `berichte` (`fromID`, `teamID`, `headline`, `content`, `occdate`, `deleted`) VALUES ('$user->id', '$teamID', '$title', '$content', '$date', '0')";
    
    if(mysql_query($query)){
        return new Msg("Erstellt", "Bericht erfolgreich erstellt!", 0);
    }else{
        return new Msg("Error", "Der Dienst hatte ein Problem, bitte versuch es später oder kontaktiere den Support!", 1);
    }
    
}

function deleteReport($user, $id){
    $query = "UPDATE `berichte` SET deleted = 1 WHERE id = ".$id;
    
    if(mysql_query($query)){
        return new Msg("Gelöscht", "Bericht erfolgreich gelöscht!", 0);
    }else{
        return new Msg("Error", "Der Dienst hatte ein Problem, bitte versuch es später oder kontaktiere den Support!", 1);
    }
}

function updateReport($user, $id, $teamID, $title, $date, $content){
    if(!$teamID || !$title || !$content || $title == "" || $content == "" || $date == ""){
        return new Msg("Fehler", "Bitte alles ausfüllen!", 1);
    }
    
    $query = "UPDATE `berichte` SET teamID = ".$teamID.", headline = '$title', content = '$content', occdate = '$date' WHERE id = ".$id." AND deleted = 0";
    
    if(mysql_query($query)){
        return new Msg("Update", "Update erfolgreich durchgeführt!", 0);
    }else{
        return new Msg("Error", "Der Dienst hatte ein Problem, bitte versuch es später oder kontaktiere den Support!", 1);
    }
}

function getReport($user, $id){
    $query = "SELECT teamID, headline, content, occdate FROM `berichte` WHERE id = $id AND deleted = 0";
    
    if($res = mysql_query($query)){
        $result = mysql_fetch_assoc($res);
        
        return '{"teamID": '.$result['teamID'].', "headline": '.json_encode( html_entity_decode ($result['headline']) ).', "date": '.json_encode( html_entity_decode ($result['occdate']) ).', "content": '.json_encode( html_entity_decode ($result['content']) ).'}';
    }else{
        return new Msg("Error", "Der Dienst hatte ein Problem, bitte versuch es später oder kontaktiere den Support!", 1);
    }
}