﻿<?php
if (!defined("IN_FUSION")) { die("Access Denied"); }

define("THEME_WIDTH", "940px");
define("THEME_BULLET", "<img border='0' src='".THEME."images/navi.gif' alt='' />");

/*function StringCutting($scString,$scMaxlength,$atspace='true')
{
  if (strlen($scString)>$scMaxlength)
  {
    $output="";
    $scString=substr($scString,0,$scMaxlength-4);
    if (strtolower($atspace)=='true')
    {
      $scStrexp=split(" ",$scString);
      for ($scI=0; $scI<count($scStrexp)-1; $scI++) $output.=$scStrexp[$scI]." ";
    }
    else $output=$scString;
    return $output."...";
  }
  else return $scString;
} */

require_once INCLUDES."theme_functions_include.php";

function render_page($license=false) {

	global $settings, $locale, $main_style, $aidlink;

	echo "<div class='container'>\n";
	
	echo "<table cellpadding='0' cellspacing='0' width='100%'>\n<tr>\n";
	echo "<td colspan='2' class='header'><img src='".THEME."images/Schriftzug+Logo.png' /></td>\n"; //".showbanners()."
	echo "</tr>\n<tr>\n";
	echo "<td class='sub-header'>".showsublinks(" ")."</td>\n";
	echo "<td class='sub-header' style='text-align:right;padding-right:6px'>".(showsubdate())."</td>\n";
	echo "</tr>\n</table>\n";
		
	echo "<table cellpadding='0' cellspacing='0' width='100%' style='margin-top:10px'>\n<tr>\n";
	if (LEFT) { echo "<td id='left-side' valign='top'><div class='side-bg'>".LEFT."<br /></div></td>\n"; }
	echo "<td id='main-content' valign='top'>".U_CENTER.CONTENT.L_CENTER."</td>\n";
	if (RIGHT) { echo "<td id='right-side' valign='top'><div class='side-bg'>".RIGHT."</div></td>\n"; }
	echo "</tr>\n</table>\n";
	
	echo "<table cellpadding='0' cellspacing='0' width='100%' style='margin-top:10px'>\n<tr>\n";
	echo "<td id='footer'>\n";
	echo "<table cellpadding='0' cellspacing='0' width='100%'>\n<tr>\n";
	echo "<td valign='top' width='50%'>".(!$license ? showcopyright()."<br />".sprintf($locale['global_172'], substr((get_microtime() - START_TIME),0,4))."" : "")." | <b>".showcounter()."</b>\n";
	echo "</td>\n";
	echo "<td style='text-align:right' valign='top'>Copyright © 2004 - 2013 | ";

  if (iADMIN && (iUSER_RIGHTS != "" || iUSER_RIGHTS != "C")) {echo "<a href='".BASEDIR."setuser.php?logout=yes'>Logout</a>";}
  else {echo "<a href='login.php'>Login</a>";}
  echo "<br />Tabellen, Spielpl&auml;ne und Ergebnisse werden aus der Datenbank<br />von <a href='http://www.sis-handball.de/web/Verein/?view=Verein&amp;VereinsNr=1310212036' target='_blank'>SIS-Handball</a> generiert und automatisch aktualisiert.";  
  
  echo "</td>\n";
	echo "</tr>\n";
	echo "</table>\n";
	echo "</td>\n</tr>\n</table>\n";

	
//	echo "<div id='footer'>\n";
//	echo "<div class='flleft' style='width:50%'>".(!$license ? showcopyright()."<br />" : "")."\n";
//	echo "Everton Duo by <a href='http://www.php-fusion.co.uk/news.php'>Nick Jones</a> based on Transparentia by <a href='http://templates.arcsin.se'>Arcsin</a></div>\n";
//	echo "<div class='flright' style='text-align:right;width:50%'>".stripslashes($settings['footer'])."</div>\n";
//	echo "<div class='clear'><br /></div>\n";
//	echo "<div class='flleft' style='width:50%'>".sprintf($locale['global_172'], substr((get_microtime() - START_TIME),0,4))."</div>\n";
//	echo "<div class='flright' style='text-align:right;width:50%'>".showcounter()."</div>\n";
//	echo "<div class='clear'></div>\n";
//	echo "</div>\n";
	
	echo "</div>\n"; // close container
	echo "<div align='center' style='width: 940px;'>
        <a href='http://validator.w3.org/check?uri=referer'><img border='0' src='".THEME."images/icon_xhtml.gif' alt='Valid XHTML 1.0 Transitional' /></a>
        <img src='".THEME."images/blind.gif' height='18' width='22' alt='' />
        <a href='http://jigsaw.w3.org/css-validator/check/referer'><img border='0' src='".THEME."images/icon_css.gif' alt='CSS ist valide!' /></a>
        <br /><img src='".THEME."images/blind.gif' height='4' width='1' alt='' /></div>";
}

function render_news($subject, $news, $info) {

	echo "<div class='item'>\n";
	echo "<h1>".$subject."</h1>\n";
	echo "<br />\n";	
//	echo "<div class='descr'>".newsposter($info,"")."</div>";
	echo $news;
	echo "<br /><div class='news-footer'>".newsposter($info,"")."".newsopts($info,"").itemoptions("N",$info['news_id'])."</div>\n";
	echo "</div>\n";

}

function render_article($subject, $article, $info) {

	echo "<div class='item'>\n";	
	echo "<h1>".$subject."</h1>\n";	
	echo "<br />\n";
	echo "".($info['article_breaks'] == "y" ? nl2br($article) : $article)."\n";
	echo "<br /><br />\n";
	echo (articleposter($info,"&middot;"));	  // utf8_decode utf8_encode
	echo " &middot; ";
	echo (articleopts($info,"&middot;").itemoptions("A",$info['article_id']));    // utf8_decode utf8_encode
	echo "</div>\n";
}

function opentable($title) {

	echo "<div class='item'>\n";
	echo "<h1>".$title."</h1>\n";

}

function opentabletrans($title) {

	echo "<div class='item'><br />\n";

}

function closetable() {

	echo "</div>\n";

}


function openside($title, $collapse = false, $state = "on") {
	
	global $panel_collapse; $panel_collapse = $collapse;
	
	if ($collapse == true) {
		$boxname = str_replace(" ", "", $title);
		echo "<div style='float:right;margin-top:11px;'>".panelbutton($state,$boxname)."</div>\n";
	}
	echo "<h1>".$title."</h1>\n";
	echo "<div class='floatfix'>\n";
	if ($collapse == true) { echo panelstate($state, $boxname); }

}

function opensidetrans($title, $collapse = false, $state = "on") {
	
	global $panel_collapse; $panel_collapse = $collapse;
	
	if ($collapse == true) {
		$boxname = str_replace(" ", "", $title);
		echo "<div style='float:right;margin-top:11px;'>".panelbutton($state,$boxname)."</div>\n";
	}
	echo "<div class='floatfix'>\n";
	if ($collapse == true) { echo panelstate($state, $boxname); }

}

function closeside($collapse = false) {

	global $panel_collapse;

	if ($panel_collapse == true) { echo "</div>\n"; }
	echo "</div>\n";	

}
?>