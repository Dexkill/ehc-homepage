<?php
require_once ("inc/functions.php");

$xmldat = ("all.xml"); 

$datei = $path.$xmldat;
$aktualisierung = filemtime($datei); 

//Ausgeben
if (file_exists($datei))
{
$xml = simplexml_load_file($datei);
//$datum = filemtime($datei);
//$datum_4tage_minus = date("Y.m.d", time()-(4*60*60*24)); 
//$datum_4tage_plus = date("Y.m.d", time()+(4*60*60*24));  


echo "<table cellspacing='0' width='100%'>";
$i = 0;

/*
0 SpielVon*
1 SpielDatum
2 SpielVon
3 LigaName
4 Heim
5 Gast
6 Tore1
7 Tore2
8 Punkte1
9 Punkte2
10 HeimNr
11 GastNr
12 Liga

(substr(($platz->SpielVon), 0, 19))
(substr(($platz->SpielDatum), 0, 10))
*/

include ("inc/selection.php");

foreach ($xml->Spiel as $platz)
 if (utf8_decode ($platz->HeimNr) == $id)  {
  {   
   $xmltoarray[] = array(strtotime (substr(($platz->SpielVon), 0, 19)), date('d.m.Y', strtotime (substr(($platz->SpielDatum), 0, 10))), date('H:i', strtotime (substr(($platz->SpielVon), 0, 19))), $platz->LigaName, $platz->Heim, $platz->Gast, $platz->Tore1, $platz->Tore2, $platz->Punkte1, $platz->Punkte2, $platz->HeimNr, $platz->GastNr, $platz->Liga );
  }
}
foreach ($xmltoarray as $key => $row) 
  {
   $Spielzeit[$key]  = $row[0];
  }

array_multisort($Spielzeit, SORT_ASC, $xmltoarray);

foreach($xmltoarray as $Wert)
    {

if (preg_match("/$sort/", $Wert[12]))
  
{
if ($i%2 != 0) {echo "<tr class='SP-hell'>";}
else {echo "<tr class='SP-dunkel'>";}
echo "<td class='SP-SpielDatum'>";
echo $Wert[1];
echo " (".$wtagekurz[date("w",strtotime ($Wert[1]))].")";
echo "</td>";
echo "<td class='SP-SpielVon'>";
//echo $Wert[2];

if ($Wert[2] == "00:00") { echo "nb*"; }
else { echo $Wert[2];
       echo " h";}

echo "</td>";
echo "<td class='SP-LigaName'>";

include ("inc/umbenennung_2.php");

echo "</td>";
echo "<td class='SP-Heim'>";

if(preg_match("/Erndtebrücker HC/u", $Wert[4]))
{ echo "<span class='ehc'>Erndtebr&uuml;cker HC</span>"; }


//if (utf8_decode ($Wert[10]) == $ehc) { echo "<span class='ehc'>Erndtebr&uuml;cker HC</span>"; }
//else if (utf8_decode ($Wert[10]) == $ehcak) { echo "<span class='ehc'>Erndtebr&uuml;cker HC</span>"; }
else { echo (StringCutting( ($Wert[4]),22,'false')); }
echo "</td>";
echo "<td class='spacer'>";
echo ":";
echo "</td>";
echo "<td class='SP-Gast'>";

if(preg_match("/Erndtebrücker HC/u", $Wert[5]))
{ echo "<span class='ehc'>Erndtebr&uuml;cker HC</span>"; }
//if (utf8_decode ($Wert[5]) == "Erndtebrücker HC") { echo "<span class='ehc'>Erndtebr&uuml;cker HC</span>"; }
//else if (utf8_decode ($Wert[5]) == "Erndtebrücker HC (a.K.)") { echo "<span class='ehc'>Erndtebr&uuml;cker HC</span>"; }
else { echo (StringCutting( ($Wert[5]),22,'false')); }
echo "</td>";
echo "<td class='SP-Tore1'>";
if ((($Wert[6]) == "0") and (($Wert[7]) == "0") and (time () < ($Wert[0]))) { echo "-"; }
else { echo $Wert[6]; }      
echo "</td>"; 
echo "<td class='spacer'>";
echo ":";
echo "</td>";
echo "<td class='SP-Tore2'>";   
if ((($Wert[7]) == "0") and (($Wert[6]) == "0") and (time () < ($Wert[0]))) { echo "-"; }
else { echo $Wert[7]; }        
echo "</td>";
echo "<td class='SP-Inf'>";
if ((($Wert[7]) == "0") and (($Wert[6]) == "0") and (time () > ($Wert[0])))
{
 echo "<a class='info' href='#'><img src='http://erndtebrueckerhc.de/cms/_sis/icons/inf.gif' border='0'><span><b>";
      if (($Wert[8]) == 2) {echo $Wert[4]; echo "</b> hat kampflos gewonnen.";}
      else if (($Wert[9]) == 2) {echo $Wert[5]; echo "</b> hat kampflos gewonnen.";}
      else if ((($Wert[8]) == 1) and (($Wert[9]) == 1)) {echo "</b>Das spiel wurde unentschieden gewertet.";}
      else {echo "</b>Das Spiel wurde noch nicht in die Datenbank von SIS-Handball eingetragen.";}
	  echo "</span></a>";
}
echo "</td>";
echo "</tr>";
$i++;
}}}
echo "</table><br />";
echo "<span class='small'><i>Letzte Aktualisierung: ";
echo date('d.m.y, H:i:s', $aktualisierung);
echo " Uhr</i></span><br /><br />";
include ("inc/footer.php");
include ("inc/legende.php");
?>