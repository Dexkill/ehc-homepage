<?php
// Zeichenkodierung
// header("Content-Type: text/html; charset=utf-8");

// Domain
$domain = "/var/www/vhosts/erndtebrueckerhc.de/httpdocs/";

// Strings
$ehc = "Erndtebrücker HC";
$ehcak = "Erndtebrücker HC (a.K.) (ak)";
$sommer = "/Sommer/";
$saison  = "Saison 2012/2013";
$saisonalt  = "Saison 2011/2012";

// Format
$team = "<b><font color='#fca000'>EHC</font> ".$name."</b>";
$teamlink = "<a href='viewpage.php?page_id=".$link."'><b>".$team."</b></a>";

// Vereinsnummer
$id = "1310212036";

// Teams
$damen = "Damen";
$herren = "Herren";
$maj = "M&auml;nnl. A-Jgd.";
$mbj = "M&auml;nnl. B-Jgd.";
$mcj = "M&auml;nnl. C-Jgd.";
$mdj = "M&auml;nnl. D-Jgd.";
$waj = "Weibl. A-Jgd.";
$wbj = "Weibl. B-Jgd.";
$wcj = "Weibl. C-Jgd.";
$wdj = "Weibl. D-Jgd.";
$gdj = "Gem. D-Jgd.";

//$gdj1 = "Gem. D-Jgd. #1";
//$gdj2 = "Gem. D-Jgd. #2";


// page_id
$id_damen = "10000";
$id_herren = "20000";
$id_maj = "40000";
$id_mbj = "41000";
$id_mcj = "42000";
$id_mdj = "43000";
$id_waj = "30000";
$id_wbj = "31000";
$id_wcj = "32000";
$id_wdj = "33000";
$id_gdj = "34000";

//$id_gdj1 = "11";
//$id_gdj2 = "50";


// Verlinkung mit EHC
$damenlink = "<a href='viewpage.php?page_id=".$id_damen."'><b><font color='#fca000'>EHC</font> ".$damen."</b></a>";
$herrenlink = "<a href='viewpage.php?page_id=".$id_herren."'><b><font color='#fca000'>EHC</font> ".$herren."</b></a>";
$majlink = "<a href='viewpage.php?page_id=".$id_maj."'><b><font color='#fca000'>EHC</font> ".$maj."</b></a>";
$mbjlink = "<a href='viewpage.php?page_id=".$id_mbj."'><b><font color='#fca000'>EHC</font> ".$mbj."</b></a>";
$mcjlink = "<a href='viewpage.php?page_id=".$id_mcj."'><b><font color='#fca000'>EHC</font> ".$mcj."</b></a>";
$mdjlink = "<a href='#'><b><font color='#fca000'>EHC</font> ".$mdj."</b></a>";
$wajlink = "<a href='viewpage.php?page_id=".$id_waj."'><b><font color='#fca000'>EHC</font> ".$waj."</b></a>";
$wbjlink = "<a href='viewpage.php?page_id=".$id_wbj."'><b><font color='#fca000'>EHC</font> ".$wbj."</b></a>";
$wcjlink = "<a href='viewpage.php?page_id=".$id_wcj."'><b><font color='#fca000'>EHC</font> ".$wcj."</b></a>";
$wdjlink = "<a href='#'><b><font color='#fca000'>EHC</font> ".$wdj."</b></a>";
$gdjlink = "<a href='viewpage.php?page_id=".$id_gdj."'><b><font color='#fca000'>EHC</font> ".$gdj."</b></a>";

//$gdjlink1 = "<a href='viewpage.php?page_id=".$id_gdj1."'><b><font color='#fca000'>EHC</font> ".$gdj1."</b></a>";
//$gdjlink2 = "<a href='viewpage.php?page_id=".$id_gdj2."'><b><font color='#fca000'>EHC</font> ".$gdj2."</b></a>";


// Verlinkung ohne EHC
$damenlink_ = "<a href='viewpage.php?page_id=".$id_damen."'>".$damen."</a>";
$herrenlink_ = "<a href='viewpage.php?page_id=".$id_herren."'>".$herren."</a>";
$majlink_ = "<a href='viewpage.php?page_id=".$id_maj."'>".$maj."</a>";
$mbjlink_ = "<a href='viewpage.php?page_id=".$id_mbj."'>".$mbj."</a>";
$mcjlink_ = "<a href='viewpage.php?page_id=".$id_mcj."'>".$mcj."</a>";
$mdjlink_ = "<a href='#'>".$mdj."</a>";
$wajlink_ = "<a href='viewpage.php?page_id=".$id_waj."'>".$waj."</a>";
$wbjlink_ = "<a href='viewpage.php?page_id=".$id_wbj."'>".$wbj."</a>";
$wcjlink_ = "<a href='viewpage.php?page_id=".$id_wcj."'>".$wcj."</a>";
$wdjlink_ = "<a href='#'>".$wdj."</a>";
$gdjlink_ = "<a href='viewpage.php?page_id=".$id_gdj."'>".$gdj."</a>";

//$gdjlink1_ = "<a href='viewpage.php?page_id=".$id_gdj1."'>".$gdj1."</a>";
//$gdjlink2_ = "<a href='viewpage.php?page_id=".$id_gdj2."'>".$gdj2."</a>";


// Pfad
$path = $domain."cms/_sis/xmlfiles/";

// Bullet
$bullet = "<img src='".BASEDIR."themes/style/images/navi.gif' alt='' />"; 

// Datum in Deutsch // lang
$wtagelang = array("Sonntag",
"Montag",
"Dienstag",
"Mittwoch",
"Donnerstag",
"Freitag",
"Samstag");

// Datum in Deutsch // kurz
$wtagekurz = array("So",
"Mo",
"Die",
"Mi",
"Do",
"Fr",
"Sa");

// zu lange Woerter kuerzen
function StringCutting($scString,$scMaxlength,$atspace='true')
{
  if (strlen($scString)>$scMaxlength)
  {
    $output="";
    $scString=substr($scString,0,$scMaxlength-4);
    if (strtolower($atspace)=='true')
    {
      $scStrexp=split(" ",$scString);
      for ($scI=0; $scI<count($scStrexp)-1; $scI++) $output.=$scStrexp[$scI]." ";
    }
    else $output=$scString;
    return $output."...";
  }
  else return $scString;
}

?>