<?php
require_once ("inc/functions.php");

$xmldat = ("letzten30.xml"); 

$datei = $path.$xmldat;
$aktualisierung = filemtime($datei); 

//Ausgeben
 
if (file_exists($datei))
{

   $xml = simplexml_load_file($datei);
   
echo "<table cellspacing='0' width='100%'>";

$i = 0;
   foreach ($xml->Spiel as $platz)
   {
      if ($i%2 != 0) {echo "<tr class='P-weis'>";}
      else {echo "<tr class='P-grau'>";}
      
      echo "<td class='P-SpielDatum'>";
      $wochentag = $wtagekurz[date("w", strtotime (substr(($platz->SpielDatum), 0, 10)))];
      echo date('d.m.y', strtotime (substr(($platz->SpielDatum), 0, 10)));
	  
      echo " ("; 
      echo $wochentag;
      echo ")</td>";
      echo "<td class='P-Heim'>";
      if (utf8_decode ($platz->HeimNr) == $id) { 


      
include ("inc/umbenennung_1.php");
     
      }
      else { echo StringCutting(($platz->Heim),22,'false'); }
      echo "</td>";
      echo "<td class='spacer'>";
      echo ":";
      echo "</td>";
      echo "<td class='P-Gast'>";
      if (utf8_decode ($platz->GastNr) == $id) { 



include ("inc/umbenennung_1.php");    
      }
      else { echo StringCutting(($platz->Gast),22,'false'); }
      echo "</td>"; 
      echo "<td class='P-Tore1'>";
      echo $platz->Tore1;
      echo "</td>"; 
      echo "<td class='spacer'>";
      echo ":";
      echo "</td>";
      echo "<td class='P-Tore2'>";
      echo $platz->Tore2; 
      echo "</td>";
      echo "<td width='6px'></td>";
      echo "<td nowrap='nowrap' class='P-HZ'>";
      if ((($platz->Tore2) == "0")   and   (($platz->Tore1) == "0")) { 
      echo "<a class='info' href='#'>Info<span><b>";
      if (($platz->Punkte1) == 2) {echo $platz->Heim; echo "</b> hat kampflos gewonnen.";}
      if (($platz->Punkte2) == 2) {echo $platz->Gast; echo "</b> hat kampflos gewonnen.";}
      if ((($platz->Punkte1) == 1) and (($platz->Punkte2) == 1)) {echo "</b>Das spiel wurde unentschieden gewertet.";}
      echo "</span></a>";
      }
      
      else if ((($platz->Tore1) == "1")   and   (($platz->Tore2) == "0") and (time() > ($platz->SpielVon))) { 
      echo "<a class='info' href='#'>Info<span><b>";
			echo $platz->Heim;
			echo "</b> hat kampflos gewonnen.";
      echo "</span></a>";
      }
      
      else if ((($platz->Tore1) == "0")   and   (($platz->Tore2) == "1") and (time() > ($platz->SpielVon))) { 
      echo "<a class='info' href='#'>Info<span><b>";
			echo $platz->Gast;
			echo "</b> hat kampflos gewonnen.";
      echo "</span></a>";
      }
      
      else {
      echo "(";
      echo "$platz->Tore01";
      echo ":";
      echo "$platz->Tore02";
      echo ")";      
      }
      echo "</td>";       
      
      echo "</tr>";
$i++;
if ($i == 5){break;} 
    }   
    echo "</table>";
    echo "<span class='small'><i>Letzte Aktualisierung: ";
    echo date('d.m.y, H:i:s', $aktualisierung);
    echo " Uhr</i></span>";
  

}

else
{
   exit('Konnte Datei nicht laden.');
}

?>