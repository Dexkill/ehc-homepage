<?php
$name = "Weibl. B-Jugend";
$link = "54";
$bericht = "4";

require_once ("inc/functions.php");

$xmltab = ("wbj_tab.xml"); 
$xmlgames = ("wbj_spielplan.xml"); 

$tab = $path.$xmltab;
$datetab = filemtime($tab); 

$games = $path.$xmlgames;
$dategames = filemtime($games);  

//echo "<img src='images/aktuell/no-foto.jpg' alt='kein teamfoto vorhanden' height='370' width='600' /><br /><br />";
echo "
<a href='images/aktuell/2013-11-wBJ.jpg' class='highslide' onclick='return hs.expand(this)'>
<img src='images/aktuell/2013-11-wBJ-small.jpg' alt='Highslide JS' title='Click to enlarge' height='524' width='700' /></a>
<br />Foto: November 2013<br /><br />
<b>Liegend:</b> Nataly Franz<br />
<b>Oben von links:</b> Trainer Christian Bonilla, Maria Römer, Maria Janson, Celine Reichmann, Julia Cofala, Lisa Weyand, Mergime Helshani, Co-Trainer Reno Kellermann<br />
<b>Unten von links:</b> Maria Zacharias, Melina Lange, Jessica Wüst, Anastasia Schlei
<br /><br />
";  

echo $bullet."<a href='articles.php?cat_id=60006'>Spielberichte</a><br />";
echo $bullet."<a href='viewpage.php?page_id=12'>Trainingszeiten</a><br /><br />";

echo "<h1>Trainerteam</h1>";
echo "Christian Bonilla<br />Reno Kellermann<br /><br />";

// Tabelle  
if (file_exists($tab))
{

   $xml = simplexml_load_file($tab);

   echo "<table class='tab-head' cellspacing='0'><tr><td>".$saison." - ";
   echo $xml->Spielklasse->Name;
   echo "</td></tr><tr><td>";   
   
$i = 0;
   foreach ($xml->Platzierung as $platz)
   {
      if (preg_match("/Erndtebrücker/", $platz->Name))  {echo "<table class='tab-ehc'>";}
//      if (utf8_decode ($platz->Name) == "Erndtebrücker HC (ak)") {echo "<table class='tab-ehc'>";}
      elseif ($i%2 != 0) {echo "<table class='tab-hell'>";}
      else {echo "<table class='tab-dunkel'>";}
      
      echo "<tr>";
      echo "<td class='tab-nr'>";
      echo $platz->Nr; 
      echo "</td>";
      echo "<td class='tab-team'>"; 

      if (preg_match("/Erndtebrücker/", $platz->Name))  {echo $team;}
//      if (utf8_decode ($platz->Name) == $ehc) {echo $team;}
      else {echo $platz->Name;}

      echo "</td>";
      echo "<td class='tab-spiele'>";
      echo $platz->Spiele;
      echo "</td>";
      echo "<td class='spacer'>";
      echo "/";
      echo "</td>";
      echo "<td class='tab-spieleinsgesammt'>";
      echo $platz->SpieleInsgesamt; 
      echo "</td>"; 
      echo "<td class='tab-toreplus'>";
      echo $platz->TorePlus;
      echo "</td>";
      echo "<td class='spacer'>";
      echo ":";
      echo "</td>";
      echo "<td class='tab-toreminus'>";
      echo $platz->ToreMinus; 
      echo "</td>"; 
      echo "<td class='tab-diff'>";
      echo $platz->D;
      echo "</td >"; 
      echo "<td class='tab-punkteplus'>";
      echo $platz->PunktePlus; 
      echo "</td>";
      echo "<td class='spacer'>";
      echo ":";
      echo "</td>";
      echo "<td class='tab-punkteminus'>";
      echo $platz->PunkteMinus;
      echo "</td>";
      echo "</tr>";
      echo "</table>";    
      
$i++;        
    }
    echo "</td></tr></table>";
    echo "<span class='small'><i>Letzte Aktualisierung der Tabelle: ";
    echo date('d.m.y, H:i:s', $datetab);
    echo " Uhr</i></span><br /><br />";
    
}
else
{
   exit('Konnte Datei nicht laden.');
} 

echo "<br />";

// Spielplan  

/*
(substr(($platz->SpielVon), 0, 19))
(substr(($platz->SpielDatum), 0, 10))
*/
if (file_exists($games))
{

   $xml = simplexml_load_file($games);


   echo "<table class='SPM-head' cellspacing='0' width='100%'><tr><td>".$saison." - ";
   echo $xml->Spielklasse->Name;
   echo "</td></tr><tr><td><table cellspacing='0'>";     

$i = 0;
   foreach ($xml->Spiel as $platz)
   {
      
    if ((utf8_decode ($platz->HeimNr) == $id) xor (utf8_decode ($platz->GastNr) == $id))  
      
      {
      if ($i%2 != 0) {echo "<tr class='SPM-hell'>";}
      else {echo "<tr class='SPM-dunkel'>";}
      echo "<td class='SPM-SpielDatum'>";
      $wochentag = $wtagekurz[date("w", strtotime (substr(($platz->SpielDatum), 0, 10)))];
      echo date('d.m.y', strtotime (substr(($platz->SpielDatum), 0, 10)));
      echo " ("; 
      echo $wochentag;
      echo ")</td>";
      echo "<td class='SPM-SpielVon' align='right'>"; 
      if (date('H:i', strtotime ($platz->SpielVon)) == "00:00") { echo "<center>--:--</center>"; }
      else { echo date('H:i', strtotime (substr(($platz->SpielVon), 0, 19)));
      echo " h";}
      echo "</td>";
      echo "<td class='SPM-Heim'>";
      if (utf8_decode ($platz->HeimNr) == $id) { echo $teamlink; }
      else { echo StringCutting(($platz->Heim),25,'false'); }
      echo "</td>";
      echo "<td class='spacer'>";
      echo ":";
      echo "</td>";
      echo "<td class='SPM-Gast'>";
      if (utf8_decode ($platz->GastNr) == $id) { echo $teamlink; }
      else { echo StringCutting(($platz->Gast),25,'false'); }
      echo "</td>"; 
      echo "<td class='SPM-Tore1'>";
      if ((($platz->Tore1) == "0") and (($platz->Tore2) == "0") and (time() > ($platz->SpielVon)))   { echo "-"; }
      else { echo $platz->Tore1; }    
      echo "</td>"; 
      echo "<td class='spacer'>";
      echo ":";
      echo "</td>";
      echo "<td class='SPM-Tore2'>";
      if ((($platz->Tore2) == "0") and (($platz->Tore1) == "0") and (time() > ($platz->SpielVon))) { echo "-"; }
      else { echo $platz->Tore2; }
      echo "</td>";
      echo "<td style='width: 16px;'>";
      echo "</td>";      
      echo "<td nowrap='nowrap' class='SPM-Spielort'>";

      if ((($platz->Tore2) == "0") and (($platz->Tore1) == "0") and (time() > ($platz->SpielVon))) { 
      echo "<a class='halle' href='#'>Halle<span>Spielst&auml;tte:<br /><b>";
      echo $platz->HallenName;
      echo "</b><br />";
      echo $platz->HallenStrasse;
      echo "<br />";      
      echo $platz->HallenOrt;
      echo "</span></a>";
      }

      else if ((($platz->Tore2) == "0")   and   (($platz->Tore1) == "0") and (time() < ($platz->SpielVon))) { 
      echo "<a class='info' href='#'>Info<span><b>";
      if (($platz->Punkte1) == 2) {echo $platz->Heim; echo "</b> hat kampflos gewonnen.";}
      if (($platz->Punkte2) == 2) {echo $platz->Gast; echo "</b> hat kampflos gewonnen.";}
      if ((($platz->Punkte1) == 1) and (($platz->Punkte2) == 1)) {echo "</b>Das spiel wurde unentschieden gewertet.";}
      echo "</span></a>";
      }

      else {
      echo "(";
      echo "$platz->Tore01";
      echo ":";
      echo "$platz->Tore02";
      echo ")";            
      }      
      echo "</td>";
      echo "</tr>"; 
$i++;
    }
    
    }
    echo "</table></td></tr></table>";
    echo "<span class='small'><i>Letzte Aktualisierung des Spielplanes: ";
    echo date('d.m.y, H:i:s', $dategames);
    echo " Uhr</i><br /><br /></span>";
    include ("inc/footer.php");
    echo "<span class='small'> Diese Daten bei <a href='http://www.sis-handball.de/web/AktuelleSeite/?view=AktuelleSeite&amp;Liga=";    
    echo $xml->Spielklasse->Liga;
    echo "' target='_blank'>SIS-Handball</a>.</span>";
//   include ("inc/legende.php");        
}
else
{
   exit('Konnte Datei nicht laden.');
} 
echo "<br />";
echo "<br />";
echo "<h2>Spielberichte-Archiv der Jugendmannschaften</h2>";
echo $bullet."<a href='articles.php?cat_id=60005'>Spielberichte 2011/2012</a><br />";
echo $bullet."<a href='articles.php?cat_id=60004'>Spielberichte 2010/2011</a><br />";
echo $bullet."<a href='articles.php?cat_id=60003'>Spielberichte 2009/2010</a><br />";
?>