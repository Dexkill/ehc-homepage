<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: latest_articles_panel.php
| Author: Nick Jones (Digitanium)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
//if (!defined("IN_FUSION")) { die("Access Denied"); }

require_once "maincore.php";
require_once THEMES."templates/header_mobil.php";
require_once INCLUDES."comments_include.php";
require_once INCLUDES."ratings_include.php";
require_once INCLUDES."theme_functions_include.php";
include LOCALE.LOCALESET."articles.php";


opentable($locale['global_030']);
$result = dbquery(
	"SELECT ta.article_id, ta.article_subject, tac.article_cat_id, tac.article_cat_access FROM ".DB_ARTICLES." ta
	INNER JOIN ".DB_ARTICLE_CATS." tac ON ta.article_cat=tac.article_cat_id
	".(iSUPERADMIN ? "" : "WHERE ".groupaccess('article_cat_access'))." AND article_draft='0' ORDER BY article_datestamp DESC LIMIT 0,12"
);
if (dbrows($result)) {
	while($data = dbarray($result)) {
		$itemsubject = trimlink($data['article_subject'], 70);
		echo THEME_BULLET." <a href='".BASEDIR."mobil_articles.php?article_id=".$data['article_id']."' title='".$data['article_subject']."'>$itemsubject</a><br />\n";
	}
} else {
	echo "<div style='text-align:center'>".$locale['global_031']."</div>\n";
}
closetable();

//require_once THEMES."templates/footer.php";
?>