Betreff:
Fotos 2/2
Von:
"Tanja Reichmann" <Reichmann.Tanja@web.de>
Datum:
26.09.2011 22:08
An:
"Volker Meier EHC" <vo.meier@gmx.de>

Soh, die n�chsten Bilder...

Bevor ich es vergesse, der Spielbericht der Damen vom 17.09. hat auf der EHC-Seite die falsche �berschrift. Dort steht "Attendorn", gespielt haben wir aber gegen "Siegen". Der Spielstand war 8:9 in der Pause, und am Ende leider 19:14. Das hatte ich leider im Bericht vergessen...

Hier die Namen der Damen

Damenmannschaft
Obere Reihe von links nach rechts: Melissa Meister, Anna Althaus, Andrea Homrighausen, Franziska Henk, Ruth Penzin, Kerstin Dickel, Tanja Reichmann, Michaela Scholz, Trainer J�rgen B�rger
Untere Reihe von links nach rechts: Theresa Trapp, Anita Becker, Anne Benfer, Katharina Langer, Jana Schmidt, Katharina Grund, Anna Dickel
Es fehlen: Kathrin Henrich, Tamara Six

Bitte tausche bei den drei Mannschaften die Trainer-Namen aus.

Die Links zum SIS fehlen noch bei allen Mannschaften. Kannst du den bitte auch �ndern?

Auf dem zweiten Foto haben wir die Neuank�mmlinge der Damen fotografiert. Bitte mit folgendem Text auf der Damenseite einf�gen: "Mit der neuen Saison gibts es beim EHC auch einige (alte) neue Gesichter: J�rgen B�rger �bernimmt das Trainieramt. Anna Dickel bestreitet ihr erstes Jahr bei den Seniorinnen; Andrea Homrighausen ist nach mehrj�hriger EHC-Pause wieder mit von der Partie."

Die Bilder der anderen Mannschaften knippse ich bei den n�chsten Heimspielen.

Viele Gr��e!
Tanja  

Ihr GMX Postfach immer dabei: die kostenlose GMX Mail App f�r Android.   
Komfortabel, sicher und schnell: www.gmx.de/android

IMG_0974.JPG


IMG_0987.JPG


Anh�nge:
IMG_0974.JPG	2.3 MB
IMG_0987.JPG	1.0 MB