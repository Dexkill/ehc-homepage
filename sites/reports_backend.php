<?php
session_start();

require_once('../user.php');
require_once('../connection.php');
require_once('../functions.php');
require_once('../msg.php');

if(!isset($_SESSION['user'])){
    $_SESSION['user'] = serialize(new User());
}

$user = unserialize($_SESSION['user']);

?>
<table class='table table-striped table-bordered reportTable'>
<thead>
    <tr><td><b>Team</b></td><td><b>Datum</b></td><td><b>Titel</b></td><td><b>Optionen</td></b></tr>
</thead>
<tbody>
<?php
if(!isset($_GET['pref'])){
    $query = "SELECT id, fromID, teamID, headline, content, createDate, occdate FROM berichte WHERE deleted = 0 ORDER BY occdate DESC";
}else if($_GET['pref'] == "4"){
    //DAMEN
    $query = "SELECT id, fromID, teamID, headline, content, createDate, occdate FROM berichte WHERE deleted = 0 AND teamID = 4 ORDER BY occdate DESC";
}else if($_GET['pref'] == "5"){
    //HERREN
    $query = "SELECT id, fromID, teamID, headline, content, createDate, occdate FROM berichte WHERE deleted = 0 AND teamID = 5 ORDER BY occdate DESC";
}else if($_GET['pref'] == "6"){
    //C JGD
    $query = "SELECT id, fromID, teamID, headline, content, createDate, occdate FROM berichte WHERE deleted = 0 AND teamID = 6 ORDER BY occdate DESC";
}

$result = mysql_query($query);

while($row = mysql_fetch_assoc($result)){
    
$queryTeamName = "SELECT name FROM teams WHERE id = ".$row['teamID'];
$resultTeamName = mysql_query($queryTeamName);
$teamNameValue = mysql_fetch_assoc($resultTeamName);
$teamName = $teamNameValue['name'];
?>
<tr>
    <td><?php echo $teamName; ?></td>
    <td><?php echo $row['occdate']; ?></td>
    <td><?php echo $row['headline']; ?></td>
    <td>
        <p class="btn btn-default" onclick="setPage('showreport|<?php echo $row['id']; ?>')"><i class="fa fa-eye"></i></p>
        <?php if($user->login){ ?>
            <p class="btn btn-warning" onclick="$('#updateReport_id').val(<?php echo $row['id']; ?>);populateUpdateMordal(<?php echo $row['id']; ?>);" data-toggle="modal" data-target="#updateReportModal"><i class="fa fa-pencil"></i></p>
            <p class="btn btn-danger pull-right" onclick="$('#deleteReport_id').val(<?php echo $row['id']; ?>)" data-toggle="modal" data-target="#deleteReportModal"><i class="fa fa-trash-o"></i></p>
        <?php } ?>
    </td>
</tr>
<?php
}
?>
</tbody>
</table>