<?php
   
require_once('../user.php');
require_once('../connection.php');
require_once('../functions.php');
require_once('../msg.php');

if(!isset($_SESSION['user'])){
    $_SESSION['user'] = serialize(new User());
}

$user = unserialize($_SESSION['user']);

$reportId = $_GET['reportId'];
$query = "SELECT teamID, headline, content, createDate, occdate FROM berichte WHERE id = $reportId AND deleted = 0";

$res = mysql_query($query);

if(mysql_num_rows($res) > 0){
    $row = mysql_fetch_assoc($res);
    
    $query1 = "SELECT name FROM teams WHERE id = ".$row["teamID"];
    $res1 = mysql_query($query1);
    $row1 = mysql_fetch_assoc($res1);
    
    $reportTeam = $row1["name"];
    $reportTitle = $row["headline"];
    $reportContent = $row["content"];
    $reportDate = $row["occdate"];
    $reportCDate = $row["createDate"];
}else{
    onError();
}

?>
<body>
    <div id="reportTeam">Team: <b><?php echo $reportTeam ?></b>; <i>Bericht-ID: <?php echo $reportId ?></i></div>
    <div id="reportHeadline"><?php echo $reportTitle ?></div>
    <div id="reportDateOcc">Am <?php echo $reportDate ?></div>
    <div id="reportContent">
        <?php echo $reportContent; ?>
    </div>
    <div id="reportDate">
        Veröffentlicht am <?php echo $reportCDate; ?>
    </div>
</body>
<?php
    function onError(){
        ?>
        <script>setPage('reports');</script>
        <?php
        echo "Page not found!";
        return;
    }
?>