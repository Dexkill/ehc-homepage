<?php
    require_once '../components/components.php';
?>
<body>
    <h1>Willkommen auf der Homepage des EHC!</h1>
    <br>
    
    <b>Die nächsten 10 Spiele:</b>
    <?php echo getNaechstenSpiele(10); ?>
    
    <b>Die letzten 10 Spiele:</b>
    <?php echo getLetzteSpiele(10); ?>
    
</body>