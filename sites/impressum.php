<?php
    require_once '../components/components.php';
?>
<body>
    
<h1>Impressum</h1>
<br>
<h2>Herausgeber:</h2>
<p><b>Erndtebrücker Handballclub 1978 e.V.</b></p><br>
<p>Handball-Kreis: <a href="http://www.lenne-sieg.de" target="_blank">Lenne-Sieg</a></p>
<p>Vereinsnummer: 1310212036</p>
<br>
<p>Online seit August 2004.</p>
<br>
<br>
<h2>Postadresse:</h2>
<p>Erndtebrücker Handballclub 1978 e.V.<br>
</p><p>Axel Jacobi</p>
<p>Geschäftsführer</p>
<p>Birkenweg 2</p>
<p>57339 Erndtebrück</p>
<br>
<p>axel(dot)jacobi(at)t-online(dot)de</p>
<br>
<p>Tel.: +49 (0) 2753 / 3850</p>
<p>Fax: +49 (0) 2753 / 604852</p>
<br>
<br>
<h2>Ersteller:</h2>
<p>Dennis Rauscher</p>
<h3>Kontakt:</h3>
<p>E-Mail: <a href="mailto:dennisrauscherd@alice.de">dennisrauscherd@alice.de</a></p>
    
</body>