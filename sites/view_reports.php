<?php
session_start();

require_once('../user.php');
require_once('../connection.php');
require_once('../functions.php');
require_once('../msg.php');

if(!isset($_SESSION['user'])){
    $_SESSION['user'] = serialize(new User());
}

$teamFilter = "";

if(isset($_GET['team']) && $_GET['team'] != ""){
    
    $choosenTeam =  mysql_real_escape_string($_GET['team']);
    
    $query = "SELECT id, name, prefab FROM teams WHERE prefab = '".$choosenTeam."'";
    $result = mysql_query($query);
    
    if(mysql_num_rows($result) > 0){
        $res = mysql_fetch_array($result);
        $teamFilter = $res['prefab'];
    }
}

$user = unserialize($_SESSION['user']);
?>
<head>
    <script>
        function loadBackend(){
            var e = document.getElementById("searchTeam");
            var strPref = e.options[e.selectedIndex].value;
            
            if(strPref == "na"){
                $("#backend").load('/sites/reports_backend.php');
            }else{
                $("#backend").load('/sites/reports_backend.php?pref=' + strPref);
            }
        }
    </script>

    <link rel="stylesheet" href="../css/style.css"/>
    <meta charset="utf-8"/>
</head>
<body>
    <div id="searchReports">
        <span>Suche nach</span>
        Team: 
        <select id="searchTeam">
            <option value="na" <?php if($teamFilter == ""){ ?>selected="selected"<?php } ?>>Alle</option>
            <?php

                $query = "SELECT id, name, prefab FROM teams";
                $result = mysql_query($query);

                while($row = mysql_fetch_assoc($result)){
            ?>
                <option value="<?php echo $row["id"]; ?>" <?php if($teamFilter != "" && $teamFilter == $row['prefab']){ ?>selected="selected"<?php } ?>><?php echo $row["name"]; ?></option>
            <?php
                }
            ?>  
        </select> 
        <input onclick="loadBackend();" class="btn btn-primary" type="submit" value="Suchen!"> 
        <?php if($user->login){ ?>
            <p class="btn btn-primary pull-right" data-toggle="modal" data-target="#createReportModal">+</p>
        <?php } ?>
    </div>
    
   <div id="backend"></div>
    
</body>
<script>loadBackend();</script>