<?php
  require_once '../components/components.php';
  ?>
  
<img src="../images/aktuell/herren_2015_2016.jpg" style="width: 100%;"/>
 <br><br>
<a onclick="setPage('trainingszeiten');" class="fa fa-clock-o" style="font-size: 18px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Trainingszeiten des gesammten Vereins"> Trainingszeiten</a>
<a onclick="setPage('reports|herren');" class="fa fa-file-text" style="font-size: 18px;margin:20px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Spielberichte"> Spielberichte</a>
<br>
<h2>Infos</h2>
<p><b>Trainer:</b> Stefan Koppelmann</p>

 <br><br><br>
  <b><i class='fa fa-futbol-o'></i> Begegnungen Saison 2015/2016</b> - 
  <?php echo getSpiele("Herren", "herren_spielplan.xml"); ?>
  <br>
  <b><i class='fa fa-table'></i> Tabelle Saison 2015/2016</b> - 
  <?php echo getTabelle("Herren", "herren_tab.xml"); ?>
  Die Tabellen, Spielpläne und Ergebnisse werden aus der Datenbank von <a href='http://www.sis-handball.de/web/Default.aspx?view=Verein&amp;VereinsNr=1310212036' target='_blank'>SIS-Handball</a> generiert und täglich fünfmal automatisch aktualisiert.
      