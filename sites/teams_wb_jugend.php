<?php
  require_once '../components/components.php';
  ?>
  
<img src="../new/images/aktuell/2013-11-wBJ-small.jpg" style="width: 100%;"/>
 <br><br>
<a onclick="setPage('trainingszeiten');" class="fa fa-clock-o" style="font-size: 18px" data-toggle="tooltip" data-placement="bottom" title="Trainingszeiten des gesammten Vereins"> Trainingszeiten</a>
 <br><br><br>
  <b><i class='fa fa-futbol-o'></i> Begegnungen Saison 2014/2015</b> - 
  <?php echo getSpiele("Weibliche B-Jugend", "wbj_spielplan.xml"); ?>
  <br>
  <b><i class='fa fa-table'></i> Tabelle Saison 2014/2015</b> - 
  <?php echo getTabelle("Weibliche B-Jugend", "wbj_tab.xml"); ?>
  Die Tabellen, Spielpläne und Ergebnisse werden aus der Datenbank von <a href='http://www.sis-handball.de/web/Default.aspx?view=Verein&amp;VereinsNr=1310212036' target='_blank'>SIS-Handball</a> generiert und täglich fünfmal automatisch aktualisiert.
      