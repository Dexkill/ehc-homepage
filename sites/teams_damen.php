<?php
  require_once '../components/components.php';
  ?>
  
<img src="../images/aktuell/Damen_2015-11.jpg" style="width: 100%;"/>
 <br><br>
<img src="../images/aktuell/damen_2015_neuzugaenge.jpg" style="width: 100%;"/>
<h4>Im neuen Trikot präsentieren sich die Neuzugänge der Saison 2015/2016 der Damenmannschaft – die beiden Neuzugänge Kira Müller und Filomena Di Rienzo sowie aus der eigenen Jugend Melina zusammen mit Trainer Jürgen Bürger.</h4>
<br><br>
<a onclick="setPage('trainingszeiten');" class="fa fa-clock-o" style="font-size: 18px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Trainingszeiten des gesammten Vereins"> Trainingszeiten</a>
<a onclick="setPage('reports|damen');" class="fa fa-file-text" style="font-size: 18px;margin:20px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Spielberichte"> Spielberichte</a>
 <br>

<h2>Infos</h2>
<p><b>Trainer:</b> Jürgen Bürger</p>

 <br><br><br>
  <b><i class='fa fa-futbol-o'></i> Begegnungen Saison 2015/2016</b> - 
  <?php echo getSpiele("Damen", "damen_spielplan.xml"); ?>
  <br>
  <b><i class='fa fa-table'></i> Tabelle Saison 2015/2016</b> - 
  <?php echo getTabelle("Damen", "damen_tab.xml"); ?>
  Die Tabellen, Spielpläne und Ergebnisse werden aus der Datenbank von <a href='http://www.sis-handball.de/web/Default.aspx?view=Verein&amp;VereinsNr=1310212036' target='_blank'>SIS-Handball</a> generiert und täglich fünfmal automatisch aktualisiert.
      