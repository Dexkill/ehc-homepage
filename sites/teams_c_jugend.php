<?php
  require_once '../components/components.php';
  ?>
  
<img src="../images/aktuell/no-foto.jpg" style="width: 100%;"/>
 <br><br>
<a onclick="setPage('trainingszeiten');" class="fa fa-clock-o" style="font-size: 18px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Trainingszeiten des gesammten Vereins"> Trainingszeiten</a>
<a onclick="setPage('reports|mcj');" class="fa fa-file-text" style="font-size: 18px;margin:20px;cursor:pointer;" data-toggle="tooltip" data-placement="bottom" title="Spielberichte"> Spielberichte</a>
<br>
<h2>Infos</h2>
<p><b>Trainer:</b> Jan Seiffert</p>

 <br><br><br>
  <b><i class='fa fa-futbol-o'></i> Begegnungen Saison 2015/2016</b> - 
  <?php echo getSpiele("Männliche C-Jugend", "mcj_spielplan.xml"); ?>
  <br>
  <b><i class='fa fa-table'></i> Tabelle Saison 2014/2015</b> - 
  <?php echo getTabelle("Männliche D-Jugend", "mcj_tab.xml"); ?>
  Die Tabellen, Spielpläne und Ergebnisse werden aus der Datenbank von <a href='http://www.sis-handball.de/web/Default.aspx?view=Verein&amp;VereinsNr=1310212036' target='_blank'>SIS-Handball</a> generiert und täglich fünfmal automatisch aktualisiert.
      