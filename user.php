<?php

class User{

    public $id   = 0;
    public $role = 0;
    public $name = "Guest";
    public $email = "";
    public $login = false;
    
    function update($id, $role, $name, $email, $login){
        $this->id = $id;
        $this->role = $role;//1 Trainer, 2 Leader, 3 Admin, 4 OP
        $this->name = $name;
        $this->email = $email;
        $this->login = $login;
    }
    
    
}