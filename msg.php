<?php

class Msg{
    
    public $type = 0; //0 = Info; 1 = Error; 2 = Warning
    public $msg = "";
    public $title = "";
    public $execute = "";
    
    function __construct($title, $msg, $type, $exec = ""){
        $this->type = $type;
        $this->msg = $msg;
        $this->title = $title;
        $this->execute = $exec;
    }
    
    public function asJson(){
        return '{"type": '.$this->type.', "title": "'.$this->title.'", "msg": "'.$this->msg.'"}';
    }
    
}