<?php
session_start();

require_once('user.php');
require_once('connection.php');
require_once('functions.php');
require_once('msg.php');

if(!isset($_SESSION['user'])){
    $_SESSION['user'] = serialize(new User());
}

$user = unserialize($_SESSION['user']);
?>

    <div id="titleContentImage">
            <a onclick="setPage('home');" href="#" id="titleBrand"><img style="width:100px;height:100px;margin:5px; float: left;" src="images/logo.png"></a>
            <div id="titleText" class="hidden-xs">Erndtebrücker Handballclub 1978 e.V.</div>
            <div id="titleTextSmall" class="visible-xs">Erndtebrücker Handballclub <br>1978 e.V.</div>
    </div>

    <nav role="navigation" class="navbar navbar-default">

        <div class="container-fluid">

            <!-- Brand and toggle get grouped for better mobile display -->

            <div class="navbar-header">

                <button type="button" data-target="#navbarCollapse" data-toggle="collapse" class="navbar-toggle">

                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                    <span class="icon-bar"></span>

                </button>

            </div>

            <!-- Collection of nav links and other content for toggling -->

            <div id="navbarCollapse" class="collapse navbar-collapse">

                <ul class="nav navbar-nav">

                    <li onclick="setPage('home');"><a href="#"><span class="glyphicon glyphicon-home"></span> Home</a></li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span class="glyphicon glyphicon-time"></span> Aktuell <b class="caret"></b></a>
                        <ul role="menu" class="dropdown-menu">
                            <li onclick="setPage('aktuell_ls');"><a href="#"><i class="fa fa-reply"></i> Letzte Spiele</a></li>
                            <li onclick="setPage('aktuell_ns');"><a href="#"><i class="fa fa-share"></i> Nächste Spiele</a></li>
                            <li class="divider"></li>
                            <li onclick="setPage('aktuell_all');"><a href="#"><i class="fa fa-bar-chart"></i> Alle Spiele</a></li>
                            <li onclick="setPage('aktuell_heim');"><a href="#"><i class="fa fa-home"></i> Alle Heimspiele</a></li>
                            <li class="divider"></li>
                        </ul>
                    </li>

                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-users"></i> Teams <b class="caret"></b></a>
                        <ul role="menu" class="dropdown-menu">
                            <li onclick="setPage('teams_damen');"><a href="#"><i class="fa fa-venus"></i> Damenmannschaft</a></li>
                            <li onclick="setPage('teams_herren');"><a href="#"><i class="fa fa-mars"></i> Herrenmannschaft</a></li>
                            <li class="divider"></li>
                            <li onclick="setPage('teams_c_jugend');"><a href="#"><i class="fa fa-mars"></i> Männliche C-Jugend</a></li>
                            <li class="divider"></li>
                        </ul>
                    </li>
                    
                     <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#"><i class="fa fa-futbol-o"></i> Verein <b class="caret"></b></a>
                        <ul role="menu" class="dropdown-menu">
                            <li onclick="setPage('reports')"><a href="#"><i class="fa fa-file-text"></i> Spielberichte</a></li>
                            <li onclick="setPage('trainingszeiten');"><a href="#"><i class="fa fa-clock-o"></i> Trainingszeiten</a></li>
                        </ul>
                    </li>
                    
                    <li onclick="setPage('anfahrt');"><a href="#"><i class="fa fa-compass"></i> Anfahrt</a></li>
                    
                    
                    
                </ul>
                
                
                <ul class="nav navbar-nav navbar-right">
                    <li onclick="setPage('haftungsausschluss');"><a href="#"><i class="fa fa-file-text-o"></i> Haftungsausschluss</a></li>
                    
                    <li onclick="setPage('impressum');"><a href="#"><i class="fa fa-credit-card"></i> Impressum</a></li>
                </ul>
                
                <?php if($user->login){ ?>
                    <ul class="nav navbar-nav navbar-right">
                        <li title="Manuelles aktualisieren der XML-Daten"><a href="/components/_save_.php?key=xlk09a_daw-1dxmnak" target="_blank"><i class="fa fa-wrench"></i> Update</a></li>
                    </ul>
                <?php } ?>
            </div>

        </div>

    </nav>
    
    <!--
    <div class="container">
        <div class="row">

            <div class="col-sm-3">
                <div class="demo-content">.col-sm-3</div>
            </div>
            <div class="col-sm-9">
                <div class="demo-content bg-alt">.col-sm-9</div>
            </div>

        </div>
    </div> -->

    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="content">
                    <div class="content-headline">
                        <div class="btn-sm btn-info glyphicon glyphicon-refresh" style="margin-bottom: 20px;cursor:pointer;" onclick="reloadPage();"></div> 
                        <span class="content-headline-text" id="header-text"></span> 
                    </div>
                    
                    <div class="content-text" id="content">
                    </div>
                </div>
            </div>
            
            <div class="col-sm-3 pull-right hidden-xs">
                <div class="content">
                    <div class="content-headline">
                        News
                    </div>
                    <div class="content-text" style="padding: 0px;">
                        
                    </div>
                </div>
            </div>
            
            <div class="col-sm-3 pull-right hidden-xs">
                <div class="content" style="margin-top: 10px;">
                    <div class="content-headline">
                        Sponsoren
                    </div>
                    <div class="content-text" style="padding: 0px;">
                        <a href="https://www.sparkasse-wittgenstein.de" target="_blank"><img class="sponsor img-thumbnail" src="images/sponsoren/sparkasse.jpg"  alt="Not found."/></a>
                        <a href="http://www.bellaitalia-erndtebrueck.de" target="_blank"><img class="sponsor img-thumbnail" src="http://www.bellaitalia-erndtebrueck.de/s/misc/logo.jpg?t=1444728755"  alt="Not found."/></a>
                        <a href="http://www.brauerei-bosch.de/" target="_blank"><img class="sponsor img-thumbnail" src="images/sponsoren/bosch.jpg"  alt="Not found."/></a>
                        <a href="http://www.knebel-holzinform.de/" target="_blank"><img class="sponsor img-thumbnail" src="http://www.knebel-holzinform.de/templates/schreinereiknebel/img/logo.png"  alt="Not found."/></a>
                        <a href="http://xn--brckenapothekeerndtebr-tlcu.apps-1and1.net/" target="_blank"><img class="sponsor img-thumbnail" src="images/sponsoren/bruecken_apotheke.jpg"  alt="Not found."/></a>
                        
                        <a href="http://www.swisslife.de/?WT.mc_id=goo_brand" target="_blank"><img class="sponsor img-thumbnail" src="https://upload.wikimedia.org/wikipedia/de/thumb/0/0c/Swiss_Life_Select_logo.svg/690px-Swiss_Life_Select_logo.svg.png"  alt="Not found."/></a>
                        
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="footer">
                    <table cellpadding='0' cellspacing='0' width='100%'>
<tr>
<td valign='top' width='50%'>
    <?php if(!$user->login){ ?>
        <p class="btn btn-default" data-toggle="modal" data-target="#loginModal">Login</p>
    <?php }else{ ?>
        <p class="btn btn-danger" data-toggle="modal" data-target="#logoutModal">Logout</p>
    <?php } ?>
</td>
<td style='text-align:right' valign='top'>Copyright © 2004 - 2015 <br />Tabellen, Spielpl&auml;ne und Ergebnisse werden aus der Datenbank<br />von <a href='http://www.sis-handball.de/web/Verein/?view=Verein&amp;VereinsNr=1310212036' target='_blank'>SIS-Handball</a> generiert und automatisch aktualisiert.</td>
</tr>
</table>
                </div>
            </div>
        </div>
    </div>
<script>
        getInnerPage();
</script>